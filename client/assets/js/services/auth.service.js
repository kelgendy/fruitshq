(function() {
	"use strict";

	angular.module('application')
	.factory('AuthService', AuthService);

	AuthService.$inject = ['$http', '$localStorage'];

	function AuthService($http, $localStorage) {
		var service = {};

		service.Login = Login;
		service.Logout = Logout;

		return service;

		function Login(username, password, callback) {
			$http.post('https://api.fruitshq.com/auth/token', { username: username, password: password })
			.then(function(response) {
				// login successful if there's a token in the response
				if (response.data.status_code == 200) {
					// store username and token in local storage to keep user logged in between page refreshes
					$localStorage.currentUser = { username: username, token: response.data.data.token };

					// add jwt token to auth header for all requests made by the $http service
					$http.defaults.headers.common.Authorization = 'Bearer ' + response.data.token;

					// execute callback with true to indicate successful login
					callback(true);
				} else {
					// execute callback with false to indicate failed login
					callback(false);
				}
			}, function(error) {
				callback(false);
			});
		}

		function Logout() {
			// remove user from local storage and clear http auth header
			delete $localStorage.currentUser;
			$http.defaults.headers.common.Authorization = '';
		}
	}
})();