(function() {
	'use strict';

	angular.module('application', [
	               'ui.router',
	               'ngAnimate',

	               'foundation',
	               'foundation.dynamicRouting',
	               'foundation.dynamicRouting.animations',

	               'ngMessages',
	               'ngStorage',
	               ])
	.config(config)
	.run(run)
	;

	config.$inject = ['$urlRouterProvider', '$locationProvider'];

	function config($urlProvider, $locationProvider) {
		$urlProvider.otherwise('/');

		$locationProvider.html5Mode({
			enabled:false,
			requireBase: false
		});

		$locationProvider.hashPrefix('!');
	}

	function run($rootScope, $http, $location, $localStorage) {
    // keep user logged in after page refresh
    if ($localStorage.currentUser) {
    	$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
    }

    // redirect to login page if not logged in and trying to access a restricted page
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
    	var publicPages = ['/login'];
    	var restrictedPage = publicPages.indexOf($location.path()) === -1;
    	if (restrictedPage && !$localStorage.currentUser) {
    		$location.path('/login');
    	}
    });
  }

})();
