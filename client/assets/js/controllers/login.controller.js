(function() {
	"use strict";

	angular.module('application')
	.controller('LoginCtrl', LoginCtrl);

	LoginCtrl.$inject = ['$location', 'AuthService'];

	function LoginCtrl($location, AuthService) {
		/* jshint validthis: true */
		var vm = this;

		vm.login = login;

		init();

		function init() {
			// reset login status
			AuthService.Logout();
		}

		function login() {
			vm.loading = true;
			AuthService.Login(vm.username, vm.password, function (result) {
				if (result === true) {
					$location.path('/');
				} else {
					vm.error = 'Username or password is incorrect';
					vm.loading = false;
				}
			});
		}
	}
})();